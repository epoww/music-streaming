#!/usr/bin/env python

import os
import socket
import struct
import sys
from threading import Lock, Thread
import math

# max client connections at once
QUEUE_LENGTH = 10

# sizes for data packets
SEND_BUFFER = 4096
SONG_ID_SIZE = 8
DATA_SIZE_SIZE = 4
COMMAND_SIZE = 9

# command constants
LIST_REQUEST = 'REQUEST 0'
PLAY_REQUEST = 'REQUEST 1'
STOP_REQUEST = 'REQUEST 2'
PLAY_NEW_REQUEST = 'REQUEST 3'

LIST_RESPONSE = 'RESPONS 0'
PLAY_RESPONSE = 'RESPONS 1'
STOP_RESPONSE = 'RESPONS 2'
PLAY_NEW_RESPONSE = 'RESPONS 3'
ERROR_RESPONSE = 'RESPONS 4'


# per-client struct
class Client:
    def __init__(self, connection, songs, songlist):
        self.lock = Lock()
        self.connection = connection

        # song metadata
        self.songs = songs
        self.songlist = songlist

        # flags for client incoming client commands
        self.recieved_play_command = False
        self.received_stop_command = False
        self.received_list_command = False

        # flag to determine whether to keep sending music or not
        self.send_music = False

        # flag for error
        self.error = False


# helper that gets the total data size and pads it with zeros
def get_total_data_size_str(prefix_len, data):
    total_data_size = str(prefix_len + len(data))
    while(len(total_data_size) < DATA_SIZE_SIZE):
        total_data_size = '0' + total_data_size
    return total_data_size


# Thread that sends music and lists to the client. Control signals from client_read are
# passed to this thread through the associated Client object. Locks are used to ensure
# that the two threads play nice with one another!
def client_write(client):
    total_song_data_sent_size, c = 0, 0
    song_id, song_data, song_data_size = None, None, 0
    reg_prefix_size = COMMAND_SIZE + DATA_SIZE_SIZE
    song_prefix_size = COMMAND_SIZE + DATA_SIZE_SIZE + SONG_ID_SIZE
    song_data_chunk_size = SEND_BUFFER - song_prefix_size

    try:
        while True:

            client.lock.acquire()
            error = client.error
            received_list_command = client.received_list_command
            send_music = client.send_music
            received_stop_command = client.received_stop_command
            client.lock.release()

            if error:
                total_data_size = get_total_data_size_str(
                    reg_prefix_size, client.errorMessage)

                data = ERROR_RESPONSE + total_data_size + client.errorMessage
                client.lock.acquire()
                client.connection.sendall(bytes(data))
                client.error = False
                client.lock.release()

            if received_list_command:
                total_data_size = get_total_data_size_str(
                    reg_prefix_size, client.songlist)

                data = LIST_RESPONSE + total_data_size + client.songlist
                client.lock.acquire()
                client.connection.sendall(bytes(data))
                client.received_list_command = False
                client.lock.release()

            if send_music:

                # different since if a new song is played we want to start from the beginning
                client.lock.acquire()

                command_str = PLAY_NEW_RESPONSE if client.recieved_play_command else PLAY_RESPONSE

                # set song id to new song id only if its a new song being played
                if client.recieved_play_command:
                    song_id_str = client.command_data
                    song_id = int(song_id_str)
                    c, total_song_data_sent_size = 0, 0
                    client.recieved_play_command = False

                # if the song doesn't exist, send error with message to client and stop music if there is any playing
                if song_id not in client.songs:
                    client.error = True
                    client.errorMessage = "Song with id " + \
                        str(song_id) + " does not exist"
                    client.send_music = False
                else:
                    song_data = client.songs[song_id][1]
                    song_data_size = len(song_data)

                    num_chunks = math.ceil(
                        song_data_size / song_data_chunk_size)
                    total_data_len = song_data_size + num_chunks * song_prefix_size

                    # if we sent the whole song, stop sending music to client
                    if total_song_data_sent_size >= total_data_len:
                        client.send_music = False
                    # otherwise, send the next chunk of data to the client
                    else:
                        i = c * song_data_chunk_size
                        song_chunk = song_data[i: i + song_data_chunk_size]

                        total_data_size = get_total_data_size_str(
                            song_prefix_size, song_chunk)

                        chunk_of_data = command_str + total_data_size + song_id_str + song_chunk
                        client.connection.sendall(bytes(chunk_of_data))
                        total_song_data_sent_size += song_data_chunk_size
                        c += 1

                client.lock.release()

            elif received_stop_command:
                total_data_size = get_total_data_size_str(
                    reg_prefix_size, '')

                data = STOP_RESPONSE + total_data_size
                c, total_song_data_sent_size = 0, 0
                song_id, song_data, song_data_size = None, None, 0
                client.lock.acquire()
                client.send_music = False
                client.connection.sendall(bytes(data))
                client.received_stop_command = False
                client.lock.release()
    except Exception:
        print("Client crashed unexpectedly")
        client.connection.close()


# Thread that receives commands from the client.
def client_read(client):
    try:
        while True:

            data = client.connection.recv(SEND_BUFFER)

            client.lock.acquire()
            command_str = data[:COMMAND_SIZE]
            client.command_data = data[COMMAND_SIZE:]

            if command_str == LIST_REQUEST:
                client.received_list_command = True
            elif command_str == PLAY_REQUEST:
                client.recieved_play_command = True
                client.send_music = True
            elif command_str == STOP_REQUEST:
                client.send_music = False
                client.received_stop_command = True
            else:
                client.error = True
                client.errorMessage = "No such command"

            client.lock.release()
    except Exception:
        print("Client crashed unexpectedly")
        client.connection.close()


def get_mp3s(musicdir):
    print("Reading music files...")
    songs = {}  # id : (song name, song data)
    songlist = '\n'

    for i, filename in enumerate(os.listdir(musicdir)):
        if not filename.endswith(".mp3"):
            continue

        # Store song metadata for future use.  You may also want to build
        # the song list once and send to any clients that need it.
        songfile = open(musicdir + filename, 'r')
        songdata = songfile.read()
        songfile.close()
        songs[i] = (filename, songdata)
        songlist += str(i) + ' ' + filename + '\n'

    print("Found {0} song(s)!".format(len(songs)))
    print(songlist)
    return songs, songlist


def main():
    if len(sys.argv) != 3:
        sys.exit("Usage: python server.py [port] [musicdir]")
    if not os.path.isdir(sys.argv[2]):
        sys.exit("Directory '{0}' does not exist".format(sys.argv[2]))

    port = int(sys.argv[1])
    songs, songlist = get_mp3s(sys.argv[2])
    threads = []

    # create a socket
    # AF_INET = address family ipv4, SOCK_STREAM = connection oriented TCP protocol.
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    print("created socket")

    # ip = empty string so server listens to requests
    server_socket.bind(('', port))
    print("binded socket to port", port)

    server_socket.listen(QUEUE_LENGTH)
    print("socket listening")

    connection = None
    while True:
        connection, address = server_socket.accept()
        print("got connection from", address)

        client = Client(connection, songs, songlist)

        t = Thread(target=client_read, args=(client,))
        threads.append(t)
        t.start()
        t = Thread(target=client_write, args=(client,))
        threads.append(t)
        t.start()

    server_socket.close()
    exit(0)


if __name__ == "__main__":
    main()
